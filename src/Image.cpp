#include "Image.hpp"
#include <fstream>
#include <iostream>
#include <cmath>
Image::Image(int largeur, int hauteur) : _largeur(largeur), _hauteur(hauteur)
{
	_pixels = new int[_largeur*_hauteur];
	for (int i=0; i<_largeur*_hauteur; i++)
	{
		_pixels[i] = 0;
	}
}

//int Image::getLargeur() const {return _largeur;}
//int Image::getHauteur() const {return _hauteur;}

const int  & Image::getLargeur() const {return _largeur;}
const int  & Image::getHauteur() const {return _hauteur;} 

const int & Image::getPixel(int i,int j) const{
	return _pixels[i*_largeur+j];
}
Image::Image(const Image &img){
	_largeur=img.getLargeur();
	_hauteur=img.getHauteur();
	_pixels = new int[_largeur*_hauteur];
	for (int i=0; i<_largeur*_hauteur; i++)
	{
		_pixels[i] = img._pixels[i];
	}
	
	}

void Image::setPixel(int i,int j,int couleur){
	 _pixels[i*_largeur+j]=couleur;
	}

int & Image::pixel(int i, int j) const
{
	return _pixels[i*_largeur+j];
}

const Image & Image::operator=(const Image & img){
	if(&img != this){
	_hauteur=img._hauteur;
	_largeur=img._largeur;
	delete[] _pixels;
	_pixels = new int[_largeur*_hauteur];
	for (int i=0; i<_largeur*_hauteur; i++)
	{
		_pixels[i] = img._pixels[i];
	}
	}
	return *this;
	}


Image::~Image(){
	delete [] _pixels;
}


void ecrirePnm(const Image &img,const std::string &nomFichier){
	std::ofstream ofs(nomFichier);
	//entete fichier
	ofs<<"P2"<<std::endl;
	ofs<<img.getLargeur()<<" "<<img.getHauteur()<<std::endl;
	ofs<<"255"<<std::endl;
	//Remplissage de pixeles
	
		for(int i=0;i<img.getHauteur();i++){
			for(int j=0;j<img.getLargeur();j++){
				ofs<<img.getPixel(i,j)<<" ";}
				ofs<<std::endl;
			}
}
void remplir(Image & img)
{
	double l = img.getLargeur();
	// Boucle contraire
	for (int j=0; j<l; j++)
	{
		double t = 5*2*M_PI*j / l;
		int c = (cos(t) + 1) * 127;
		for (int i=0; i<img.getHauteur(); i++)
		{
			img.pixel(i,j) = c;
		}
	}
}

Image bordure(const Image & img, int couleur){
	
	Image img2 = img;

	const int lastCol = img2.getLargeur()-1;
	for(int i=0; i<img2.getHauteur(); i++)
	{
		img2.pixel(i,0) = couleur;
		img2.pixel(i, lastCol) = couleur;
	}

	const int lastLine = img2.getHauteur()-1;
	for (int j=0; j<img2.getLargeur(); j++)
	{
		img2.pixel(0, j) = couleur;
		img2.pixel(lastLine, j) = couleur;
	}
	return img2;

		}

