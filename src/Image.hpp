#ifndef IMAGE_HPP_
#define IMAGE_HPP_
#include <vector>
#include <string>

class Image {
	private:
int _largeur;
int _hauteur;
int *_pixels;
	public:
Image(int hauteur,int largeur);
Image(const Image &img);
const Image & operator=(const Image & img);
//int getLargeur() const;
//int getHauteur() const;
int & pixel(int i, int j) const;
const int & getLargeur() const;
const int & getHauteur() const;

const int & getPixel(int i,int j) const;
void setPixel(int i,int j, int couleur);
~Image();

};
void ecrirePnm(const Image &img,const std::string &nomFichier);
void remplir(Image &img);
Image bordure(const Image & img, int couleur);
#endif
